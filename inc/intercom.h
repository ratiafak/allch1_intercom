/*
 * intercom.h
 *
 *  Created on: Jan 26, 2018
 *      Author: ratiafak
 */

#ifndef INTERCOM_H_
#define INTERCOM_H_

#include <stdint.h>

#define INTERCOM_BUFFER_SIZE   64	//Words of uint32_t

typedef volatile struct
{
	uint32_t* ptr_SystemCoreClock;	//Pointer to SystemCoreClock

	uint32_t lli0_address;	//Address of the first linked register of GPDMA, comparison with LPC_GPDMA->C0LLI tells which buffer is ready
	uint32_t* buffer0;	//Pointer to first buffer, where ADC values are read
	uint32_t* buffer1;	//Pointer to second buffer, where ADC values are read
	int16_t* fft_out;	//Pointer to the FFTed data
	int16_t* mag;	//Pointer to the magnitude data
	int16_t* mag_last;	//Pointer to the last magnitude data
	uint32_t mag_channels;	//Set to 32, if second half of magnitude was calculated
} intercom_t;

extern intercom_t intercom;

#endif /* INTERCOM_H_ */
